﻿using GiftList.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;


namespace GiftList.Filters
{
    public class MenuActionFilter : ActionFilterAttribute
    {

        private readonly IGListRepository _gListRepository;
        public MenuActionFilter(IGListRepository gListRepository)
        {
            _gListRepository = gListRepository;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.Controller is Controller)
            {
                Controller controller = context.Controller as Controller;
                controller.ViewBag.Title = "Lista prezentów";
                controller.ViewBag.Glists = _gListRepository.LoadLists();
            }
        }
    }
}
