﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GiftList.Migrations
{
    public partial class BookedBy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBooked",
                table: "Gifts");

            migrationBuilder.AddColumn<string>(
                name: "BookedBy",
                table: "Gifts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BookedBy",
                table: "Gifts");

            migrationBuilder.AddColumn<bool>(
                name: "IsBooked",
                table: "Gifts",
                nullable: false,
                defaultValue: false);
        }
    }
}
