﻿// <auto-generated />
using System;
using GiftList.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace GiftList.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20200404163028_gift-list")]
    partial class giftlist
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.11-servicing-32099")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("GiftList.Models.Gift", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.Property<int?>("GListId");

                    b.Property<string>("ImageUrl");

                    b.Property<bool>("IsBooked");

                    b.Property<string>("Link");

                    b.Property<string>("Name");

                    b.Property<double>("Price");

                    b.HasKey("Id");

                    b.HasIndex("GListId");

                    b.ToTable("Gifts");
                });

            modelBuilder.Entity("GiftList.Models.GList", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("OwnersName");

                    b.HasKey("Id");

                    b.ToTable("GLists");
                });

            modelBuilder.Entity("GiftList.Models.Gift", b =>
                {
                    b.HasOne("GiftList.Models.GList", "GList")
                        .WithMany("Gifts")
                        .HasForeignKey("GListId");
                });
#pragma warning restore 612, 618
        }
    }
}
