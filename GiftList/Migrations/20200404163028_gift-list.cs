﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GiftList.Migrations
{
    public partial class giftlist : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GListId",
                table: "Gifts",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OwnersName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GLists", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Gifts_GListId",
                table: "Gifts",
                column: "GListId");

            migrationBuilder.AddForeignKey(
                name: "FK_Gifts_GLists_GListId",
                table: "Gifts",
                column: "GListId",
                principalTable: "GLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Gifts_GLists_GListId",
                table: "Gifts");

            migrationBuilder.DropTable(
                name: "GLists");

            migrationBuilder.DropIndex(
                name: "IX_Gifts_GListId",
                table: "Gifts");

            migrationBuilder.DropColumn(
                name: "GListId",
                table: "Gifts");
        }
    }
}
