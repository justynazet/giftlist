﻿using GiftList.Filters;
using GiftList.Models;
using GiftList.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GiftList.Controllers
{

    public class GiftController : Controller
    {
        private readonly IGiftRepository _giftRepository;
        private readonly IGListRepository _gListRepository;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IHostingEnvironment _env;

        public GiftController(IGiftRepository giftRepository, UserManager<IdentityUser> userManager, IGListRepository gListRepository, IHostingEnvironment env)
        {
            _gListRepository = gListRepository;
            _giftRepository = giftRepository;
            _userManager = userManager;
            _env = env;
        }


        public IActionResult GiftsToList(int id, int? low, int? high)
        {
            var gifts = _giftRepository.LoadGifts(id, low, high).OrderBy(g => g.Name);
            var giftVM = new GiftVM()
            {
                Gifts = gifts.ToList(),
                LowFilter = low,
                HighFilter = high
            };
            return View(giftVM);
        }

        [HttpGet("/Gift/Reservation/{id}", Name = "reservation")]
        public IActionResult Reservation(int id)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            _giftRepository.Book(id, userId);
            return Json(new
            {
                success = true
            });
        }

        [HttpGet("/Gift/Cancel/{id}", Name = "cancel")]
        public IActionResult Cancel(int id)
        {
            _giftRepository.Cancel(id);
            return Json(new
            {
                success = false
            });
        }

        [ServiceFilter(typeof(MenuActionFilter))]
        [HttpGet("Gift/Owner/{id}", Name = "owner")]
        public IActionResult Owner(int id, int? low, int? high)
        {
            GiftsToList(id, low, high);
            return View("Index");
        }

        [ServiceFilter(typeof(MenuActionFilter))]
        public IActionResult CreateList()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(MenuActionFilter))]
        public IActionResult CreateList(GList gList)
        {
            if (ModelState.IsValid)
            {
                _gListRepository.AddList(gList);
                return RedirectToAction("Owner", new { id = gList.Id });
            }
            return View(gList);
        }

        [Authorize(Roles = "Administrator")]
        [ServiceFilter(typeof(MenuActionFilter))]
        public IActionResult Create(string FileName)
        {
            if (!string.IsNullOrEmpty(FileName))
                ViewBag.ImgPath = "/images/" + FileName;
            return View();
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(MenuActionFilter))]
        public IActionResult Create(Gift gift)
        {
            if (ModelState.IsValid)
            {
                _giftRepository.AddGift(gift);
                return RedirectToAction("Owner", new { id = gift.GListId });
            }
            return View(gift);
        }

        [Authorize(Roles = "Administrator")]
        [ServiceFilter(typeof(MenuActionFilter))]
        public IActionResult Edit(int Id, string FileName)
        {
            var gift = _giftRepository.LoadGiftById(Id);

            if (gift == null)
                return NotFound();

            if (!string.IsNullOrEmpty(FileName))
                ViewBag.ImgPath = "/images/" + FileName;
            else
                ViewBag.ImgPath = gift.ImageUrl;

            return View(gift);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(MenuActionFilter))]
        public IActionResult Edit(Gift gift)
        {
            if (ModelState.IsValid)
            {
                _giftRepository.EditGift(gift);
                return RedirectToAction("Owner", new { id = gift.GListId });
            }
            return View(gift);
        }

        [Authorize(Roles = "Administrator")]
        [ServiceFilter(typeof(MenuActionFilter))]
        public IActionResult Delete(int Id)
        {
            var gift = _giftRepository.LoadGiftById(Id);

            if (gift == null)
                return NotFound();

            return View(gift);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(MenuActionFilter))]
        public IActionResult DeleteConfirmed(int id, string imageUrl)
        {
            var gift = _giftRepository.LoadGiftById(id);
            _giftRepository.DeleteGift(gift);
            if (imageUrl != null)
            {
                var webRoot = _env.WebRootPath;
                var path = Path.Combine(webRoot.ToString() + imageUrl);
                System.IO.File.Delete(path);
            }

            return RedirectToAction("Owner", new { id = gift.GListId });
        }

        [HttpPost("UploadFile")]
        public async Task<IActionResult> UploadFile(IFormCollection form)
        {
            var webRoot = _env.WebRootPath;
            var filePath = Path.Combine(webRoot.ToString() + "\\images\\" + form.Files[0].FileName);

            if (form.Files[0].FileName.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await form.Files[0].CopyToAsync(stream);
                }
            }
            if (Convert.ToString(form["Id"]) == string.Empty || Convert.ToString(form["Id"]) == "0")
            {
                return RedirectToAction("Create", new { FileName = Convert.ToString(form.Files[0].FileName) });
            }
            return RedirectToAction("Edit", new { FileName = Convert.ToString(form.Files[0].FileName), id = Convert.ToString(form["Id"])});
        }
    }
}
