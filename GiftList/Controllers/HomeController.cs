﻿using GiftList.Filters;
using Microsoft.AspNetCore.Mvc;



namespace GiftList.Controllers
{
    [ServiceFilter(typeof(MenuActionFilter))]
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }
    }

}
