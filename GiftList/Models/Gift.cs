﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiftList.Models
{
    public class Gift
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public double Price { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public int GListId { get; set; }
        public virtual GList GList { get; set; }
        public string BookedBy { get; set; }
    }
}
