﻿using GiftList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiftList.Models
{
    public interface IGiftRepository
    {
        IEnumerable<Gift> LoadGifts(int id, int? low, int? high);
        Gift LoadGiftById(int Id);
        void Book(int giftId, string userId);
        void Cancel(int giftId);
        void AddGift(Gift gift);
        void EditGift(Gift gift);
        void DeleteGift(Gift gift);
    }
}
