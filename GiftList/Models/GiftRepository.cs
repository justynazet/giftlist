﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GiftList.Models
{
    public class GiftRepository : IGiftRepository
    {

        private readonly AppDbContext _appDbContext;
        private readonly UserManager<IdentityUser> _userManager;

        public GiftRepository(AppDbContext appDbContext, UserManager<IdentityUser> userManager)
        {
            _appDbContext = appDbContext;
            _userManager = userManager;
        }

        public IEnumerable<Gift> LoadGifts(int glistid, int? low, int? high)
        {
            IEnumerable<Gift> result = _appDbContext.Gifts.Include(a => a.GList);
            result = result.Where(a => a.GList.Id == glistid);
            if (low.HasValue)
            {
                result = result.Where(g => g.Price >= low);
            }
            if (high.HasValue)
            {
                result = result.Where(g => g.Price < high);
            }

            return result;
        }

        public Gift LoadGiftById(int giftId)
        {
            return _appDbContext.Gifts.FirstOrDefault(g => g.Id == giftId);
        }

        public void Book(int giftId, string userId)
        {
            var gift = _appDbContext.Gifts.FirstOrDefault(g => g.Id == giftId);
            gift.BookedBy = userId;
            _appDbContext.SaveChanges();
        }

        public void Cancel(int giftId)
        {
            var gift = _appDbContext.Gifts.FirstOrDefault(g => g.Id == giftId);
            gift.BookedBy = null;
            _appDbContext.SaveChanges();
        }

        public void AddGift(Gift gift)
        {
            _appDbContext.Gifts.Add(gift);
            _appDbContext.SaveChanges();
        }

        public void EditGift(Gift gift)
        {
            _appDbContext.Gifts.Update(gift);
            _appDbContext.SaveChanges();
        }

        public void DeleteGift(Gift gift)
        {
            _appDbContext.Gifts.Remove(gift);
            _appDbContext.SaveChanges();
        }
    }
}
