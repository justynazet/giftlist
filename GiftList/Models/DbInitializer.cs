﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiftList.Models
{
    public static class DbInitializer
    {

        
        public static void Seed(AppDbContext context)
        {
            if (!context.Gifts.Any())

            {
                context.Add(
                    new GList
                    {
                        OwnersName = "Ryszard",
                        Gifts = new List<Gift>
                        {
                            new Gift { Name = "Model samochodu", ImageUrl = "/images/car.png", Price = 29.99, Link = "http://smyk.pl/car", BookedBy = null, Description = "Metalowy model samochodu, skala 1:16"},
                            new Gift { Name = "Książka po angielsku", ImageUrl = "/images/book.png", Price = 17.25, Link = "http://smyk.pl/book", BookedBy = null, Description = "Książka do nauki angielskiego dla małego dziecka" },
                            new Gift { Name = "Lego Minecraft", ImageUrl = "/images/bricks.png", Price = 40.50, Link = "http://smyk.pl/bricks", BookedBy = null, Description = "Lego z serii Minecraft, może być inny zestaw" },
                            new Gift { Name = "Spodnie", ImageUrl = "/images/trousers.png", Price = 70.50, Link = "http://smyk.pl/trousers", BookedBy = null, Description = "Miękkie spodnie, rozmiar 110" },
                            new Gift { Name = "Koszula", ImageUrl = "/images/shirt.png", Price = 57.50, Link = "http://smyk.pl/shirt", BookedBy = null, Description = "Koszula elegancka albo bardziej casualowa, rozmiar 110" },
                            new Gift { Name = "Piłka", ImageUrl = "/images/ball.png", Price = 17.50, Link = "http://smyk.pl/ball", BookedBy = null, Description = "Piłka nożna" }
                        }
                    }
                );

                context.Add(
                    new GList
                    {
                        OwnersName = "Wanda",
                        Gifts = new List<Gift>
                        {
                            new Gift { Name = "Lalka", ImageUrl = "/images/doll.png", Price = 139.99, Link = "http://smyk.pl/doll", BookedBy = null, Description = "Miękka lalka do przytulania" },
                            new Gift { Name = "Puzzle", ImageUrl = "/images/puzzle.png", Price = 15.65, Link = "http://smyk.pl/puzzle", BookedBy = null, Description = "Puzzle dla małego dziecka z prostym dopasowaniem" },
                            new Gift { Name = "Miś Pluszowy", ImageUrl = "/images/teddybear.png", Price = 42.50, Link = "http://smyk.pl/teddybear", BookedBy = null, Description = "Pluszowy miś, może być inny kolor, max wysokość 80 cm" },
                            new Gift { Name = "Domek dla lalek", ImageUrl = "/images/dollhouse.png", Price = 42.50, Link = "http://smyk.pl/dollhouse", BookedBy = null, Description = "Domek dla lalek barbie, piętrowy" },
                            new Gift { Name = "Bluza", ImageUrl = "/images/blouse.png", Price = 34.49, Link = "http://smyk.pl/blouse", BookedBy = null, Description = "Blua sportowa, rozmiar 92" },
                            new Gift { Name = "Sukienka", ImageUrl = "/images/dress.png", Price = 159.99, Link = "http://smyk.pl/dress", BookedBy = null, Description = "Letnia sukienka, najlepiej dżinsowa" }

                        }
                    }
                );
            }
            context.SaveChanges();

        }
    }
}
