﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiftList.Models
{
    public interface IGListRepository
    {
        GList LoadListByOwner(int id);
        IEnumerable<GList> LoadLists();
        void AddList(GList gList);
    }
}
