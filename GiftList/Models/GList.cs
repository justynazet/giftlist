﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiftList.Models
{
    public class GList
    {
        public int Id { get; set; }
        public string OwnersName { get; set; }
        public virtual ICollection<Gift> Gifts { get; set; }
    }
}
