﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiftList.Models
{
    public class GListRepository : IGListRepository
    {
        private readonly AppDbContext _appDbContext;
        
        public GListRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public void AddList(GList gList)
        {
            _appDbContext.GLists.Add(gList);
            _appDbContext.SaveChanges();
        }

        public GList LoadListByOwner(int id)
        {
            return _appDbContext.GLists.FirstOrDefault(g => g.Id == id);
        }

        public IEnumerable<GList> LoadLists()
        {
            IEnumerable<GList> result = _appDbContext.GLists;
            return result;
        }
    }
}
