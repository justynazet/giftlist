﻿using GiftList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiftList.ViewModels
{
    public class GiftVM 
    {
        public List<Gift> Gifts { get; set; }
        public int? LowFilter { get; set; }
        public int? HighFilter { get; set; }       
    }
}
